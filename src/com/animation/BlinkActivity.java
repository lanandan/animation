package com.animation;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

public class BlinkActivity extends ActionBarActivity {
	TextView txt_slidUp;
	Animation animBlink;
	ImageView img_slide;
	View v;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_blink);
		animBlink = AnimationUtils.loadAnimation(getApplicationContext(),
				R.anim.blink);

		txt_slidUp = (TextView) findViewById(R.id.blink_in);
		img_slide = (ImageView) findViewById(R.id.img_blink);

		img_slide.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				img_slide.startAnimation(animBlink);
			}
		});

	}
	
	
	
	public void onclick(View v){
		Intent fadeIn=new Intent();
		switch (v.getId()) {
		case R.id.btn_bl_preview:
			fadeIn.setClass(BlinkActivity.this, BounceActivity.class);
			startActivity(fadeIn);
			overridePendingTransition(R.anim.left_out, R.anim.right_in);
			break;
			
		case R.id.btn_bl_next:
			fadeIn.setClass(BlinkActivity.this, ZoomInActivity.class);
			startActivity(fadeIn);
			overridePendingTransition(R.anim.right_in, R.anim.left_out);
			break;

		default:
			break;
		}
	}

}
