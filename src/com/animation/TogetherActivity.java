package com.animation;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

public class TogetherActivity extends ActionBarActivity {
	TextView txt_slidUp;
	Animation animSequential;
	ImageView img_slide;
	View v;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_together_animation);
		animSequential = AnimationUtils.loadAnimation(getApplicationContext(),
				R.anim.together_animation);

		txt_slidUp = (TextView) findViewById(R.id.together);
		img_slide = (ImageView) findViewById(R.id.img_together);

		img_slide.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				img_slide.startAnimation(animSequential);
			}
		});

	}
	
	
	
	public void onclick(View v){
		Intent fadeIn=new Intent();
		switch (v.getId()) {
		case R.id.btn_to_preview:
			fadeIn.setClass(TogetherActivity.this, SequentialActivity.class);
			startActivity(fadeIn);
			overridePendingTransition(R.anim.left_out, R.anim.right_in);
			break;
			
		

		default:
			break;
		}
	}
}
