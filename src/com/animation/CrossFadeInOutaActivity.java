package com.animation;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

public class CrossFadeInOutaActivity extends ActionBarActivity {
	TextView txt_fadeOut;
	Animation animFadeOut, animFadeIn;
	ImageView img_crossFading;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_cross_fade);

		animFadeOut = AnimationUtils.loadAnimation(getApplicationContext(),
				R.anim.fade_out);
		animFadeIn = AnimationUtils.loadAnimation(getApplicationContext(),
				R.anim.fade_in);

		txt_fadeOut = (TextView) findViewById(R.id.cross_fadeInOut);
		img_crossFading = (ImageView) findViewById(R.id.img_cross);
		img_crossFading.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				img_crossFading.startAnimation(animFadeOut);

				img_crossFading.setBackgroundDrawable(getResources()
						.getDrawable(R.drawable.bank));
				img_crossFading.startAnimation(animFadeIn);

			}
		});

	}
	
	
	public void onclick(View v){
		Intent fadeIn=new Intent();
		switch (v.getId()) {
		case R.id.btn_cr_preview:
			fadeIn.setClass(CrossFadeInOutaActivity.this, FadeIOutActivity.class);
			startActivity(fadeIn);
			overridePendingTransition(R.anim.left_out, R.anim.right_in);
			break;
			
		case R.id.btn_cr_next:
			fadeIn.setClass(CrossFadeInOutaActivity.this, SlideUpActivity.class);
			startActivity(fadeIn);
			overridePendingTransition(R.anim.right_in, R.anim.left_out);
			break;

		default:
			break;
		}
	}

}
