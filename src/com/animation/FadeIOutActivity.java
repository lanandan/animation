package com.animation;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

public class FadeIOutActivity extends ActionBarActivity {
	TextView txt_fadeOut;
	Animation animFadeOut,animFadeIn;
	ImageView img_fadeOut;

	View v;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_fade_out);
		animFadeOut = AnimationUtils.loadAnimation(getApplicationContext(),
				R.anim.fade_out);
		animFadeIn = AnimationUtils.loadAnimation(getApplicationContext(),
				R.anim.fade_in);
		
		
		txt_fadeOut = (TextView) findViewById(R.id.fade_out);
		img_fadeOut = (ImageView) findViewById(R.id.img_fade_out);
		txt_fadeOut.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				txt_fadeOut.startAnimation(animFadeOut);
				txt_fadeOut.setText("Icon Fade Out"); 
				txt_fadeOut.setTextColor(getResources().getColor(R.color.blue));
				img_fadeOut.setBackgroundDrawable(getResources().getDrawable(R.drawable.fadeout));
				

			}
		});
		img_fadeOut.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				img_fadeOut.startAnimation(animFadeOut);
				txt_fadeOut.setText("Apple Icon");
				txt_fadeOut.setTextColor(getResources().getColor(R.color.dbr));
				img_fadeOut.setBackgroundDrawable(getResources().getDrawable(R.drawable.fadeoutafter));
				img_fadeOut.startAnimation(animFadeOut);
				
			}
		});


		
	}

	
	public void onclick(View v){
		Intent fadeIn=new Intent();
		switch (v.getId()) {
		case R.id.btn_fo_preview:
			fadeIn.setClass(FadeIOutActivity.this, FadeInActivity.class);
			startActivity(fadeIn);
			overridePendingTransition(R.anim.left_out, R.anim.right_in);
			break;
			
		case R.id.btn_fo_next:
			fadeIn.setClass(FadeIOutActivity.this, CrossFadeInOutaActivity.class);
			startActivity(fadeIn);
			overridePendingTransition(R.anim.right_in, R.anim.left_out);
			break;

		default:
			break;
		}
	}

	
}
