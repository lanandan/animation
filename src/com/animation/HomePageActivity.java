package com.animation;


import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class HomePageActivity extends Activity {

	
	TextView txt_anim;
	// hello;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);
		
		txt_anim=(TextView)findViewById(R.id.txt_animation);
		txt_anim.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			Intent animationstart=new Intent(HomePageActivity.this,FadeInActivity.class);	
			startActivity(animationstart);
			overridePendingTransition(R.anim.right_in, R.anim.left_out);
			}
		});
	}

}
