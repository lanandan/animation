package com.animation;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

public class BounceActivity extends ActionBarActivity {
	TextView txt_slidUp;
	Animation animSlideDown;
	ImageView img_slide;
	View v;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_bounce);
		animSlideDown = AnimationUtils.loadAnimation(getApplicationContext(),
				R.anim.slide_down);

		txt_slidUp = (TextView) findViewById(R.id.bounce_in);
		img_slide = (ImageView) findViewById(R.id.img_bounce);

		img_slide.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				img_slide.startAnimation(animSlideDown);
			}
		});

	}
	
	
	
	public void onclick(View v){
		Intent fadeIn=new Intent();
		switch (v.getId()) {
		case R.id.btn_bu_preview:
			fadeIn.setClass(BounceActivity.this, SlideDownActivity.class);
			startActivity(fadeIn);
			overridePendingTransition(R.anim.left_out, R.anim.right_in);
			break;
			
		case R.id.btn_bu_next:
			fadeIn.setClass(BounceActivity.this, BlinkActivity.class);
			startActivity(fadeIn);
			overridePendingTransition(R.anim.right_in, R.anim.left_out);
			break;

		default:
			break;
		}
	}

}
