package com.animation;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

public class SequentialActivity extends ActionBarActivity {
	TextView txt_slidUp;
	Animation animSequential;
	ImageView img_seq,img_se1,imgse3;
	View v;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sequential);
		animSequential = AnimationUtils.loadAnimation(getApplicationContext(),
				R.anim.sequential_animation);

		txt_slidUp = (TextView) findViewById(R.id.txt_sequ);
		img_seq = (ImageView) findViewById(R.id.img_seq);
		img_se1 = (ImageView) findViewById(R.id.img_seq1);
	
		img_seq.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				img_seq.startAnimation(animSequential);
				img_se1.startAnimation(animSequential);
				
			}
		});
		img_se1.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				img_seq.startAnimation(animSequential);
				img_se1.startAnimation(animSequential);
				
			}
		});

	}
	
	
	
	public void onclick(View v){
		Intent fadeIn=new Intent();
		switch (v.getId()) {
		case R.id.btn_sq_preview:
			fadeIn.setClass(SequentialActivity.this, RotateActivity.class);
			startActivity(fadeIn);
			overridePendingTransition(R.anim.left_out, R.anim.right_in);
			break;
			
		case R.id.btn_sq_next:
			fadeIn.setClass(SequentialActivity.this, TogetherActivity.class);
			startActivity(fadeIn);
			overridePendingTransition(R.anim.right_in, R.anim.left_out);
			break;

		default:
			break;
		}
	}
}
