package com.animation;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

public class FadeInActivity extends ActionBarActivity {
	TextView txt_fadeIn;
	Animation animFadeIn;
	ImageView img_fade;
	View v;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		animFadeIn = AnimationUtils.loadAnimation(getApplicationContext(),
				R.anim.fade_in);

		txt_fadeIn = (TextView) findViewById(R.id.fade_in);
		img_fade = (ImageView) findViewById(R.id.img_fadein);
		txt_fadeIn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				txt_fadeIn.startAnimation(animFadeIn);
				txt_fadeIn.setText("Civil");
				txt_fadeIn.setTextColor(getResources().getColor(R.color.blue));
				img_fade.setBackgroundDrawable(getResources().getDrawable(R.drawable.cvil));

			}
		});

		
		img_fade.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				img_fade.startAnimation(animFadeIn);
				txt_fadeIn.setText("CIVIL Engg Build");
				txt_fadeIn.setTextColor(getResources().getColor(R.color.dbr));
				img_fade.setBackgroundDrawable(getResources().getDrawable(R.drawable.cvilbuild));
				
			}
		});

	}
	
	public void onclick(View v){
		Intent fadeIn=new Intent();
		switch (v.getId()) {
		case R.id.btn_fi_preview:
			fadeIn.setClass(FadeInActivity.this, HomePageActivity.class);
			startActivity(fadeIn);
			overridePendingTransition(R.anim.left_out, R.anim.right_in);
			break;
			
		case R.id.btn_fi_next:
			fadeIn.setClass(FadeInActivity.this, FadeIOutActivity.class);
			startActivity(fadeIn);
			overridePendingTransition(R.anim.right_in, R.anim.left_out);
			break;

		default:
			break;
		}
	}

	
	
}
