package com.animation;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

public class SlideUpActivity extends ActionBarActivity {
	TextView txt_slidUp;
	Animation animSlideUp, animSlidDown;
	ImageView img_slide;
	View v;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_slide_up);
		animSlideUp = AnimationUtils.loadAnimation(getApplicationContext(),
				R.anim.slide_up);
		animSlidDown = AnimationUtils.loadAnimation(getApplicationContext(),
				R.anim.slide_down);

		txt_slidUp = (TextView) findViewById(R.id.slide_up);
		img_slide = (ImageView) findViewById(R.id.img_slideup);

		img_slide.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				img_slide.startAnimation(animSlideUp);
				img_slide.setBackgroundDrawable(getResources().getDrawable(
						R.drawable.garden));
				img_slide.startAnimation(animSlidDown);
			}
		});

	}

	public void onclick(View v) {
		Intent fadeIn = new Intent();
		switch (v.getId()) {
		case R.id.btn_su_preview:
			fadeIn.setClass(SlideUpActivity.this, CrossFadeInOutaActivity.class);
			startActivity(fadeIn);
			overridePendingTransition(R.anim.left_out, R.anim.right_in);
			break;

		case R.id.btn_su_next:
			fadeIn.setClass(SlideUpActivity.this, SlideDownActivity.class);
			startActivity(fadeIn);
			overridePendingTransition(R.anim.right_in, R.anim.left_out);
			break;

		default:
			break;
		}
	}

}